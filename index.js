const net = require('net');

const { CLI } = require('./cli/cli');
const { createTcpListener, destroyTcpListener } = require('./net/tcpClient');
const { attachTcpEventEmitters } = require('./net/tcpEvents');
const { EventBus } = require('./helpers/eventBus');
const { EVENTS } = require('./helpers/constants');
const { buildObjectAsciiTable } = require('./helpers/buildObjectAsciiTable');
const { timeReponseIsRandomHigh } = require('./net/responseParsers/timeResponseIsRandomHigh');
const { formatTcpJsonString } = require('./helpers/formatTcpJsonString');
const {
  regSession, pauseAllSessionsButUser, resumeAllSessions, getSessionByUser,
  removeSession, getSessionsUserList, isCurrentFilterUser,
} = require('./net/tcpClientSessionManger');

let verbose = false;

EventBus.on(EVENTS.CLI.FILTER_USER, (filterUser) => {
  // This is just in case for some reason something gets passed the logged in
  // filter logic during the command action.
  if (!getSessionsUserList().includes(filterUser)) {
    CLI.log('Requested user has no active session, cannot filter by provided user.');
    return;
  }
  try {
    pauseAllSessionsButUser(filterUser);
  } catch (err) {
    CLI.log(err.message);
  }
});

EventBus.on(EVENTS.CLI.FILTER_USER_CLEAR, () => {
  resumeAllSessions();
});

EventBus.on(EVENTS.TCP_CLIENT.WRITE_TO, (objArr, user, lineEndings = true) => {
  try {
    const writeString = formatTcpJsonString(objArr, lineEndings);
    const socket = getSessionByUser(user);
    socket.write(writeString);
  } catch (err) {
    EventBus.emit(EVENTS.TCP_CLIENT.WRITE_ERROR, err, user);
  }
});

EventBus.on(EVENTS.TCP_CLIENT.READ_SUCCESS, (dataObj, user) => {
  const isCurrentFilter = isCurrentFilterUser(user);
  const userSocket = getSessionByUser(user);

  /* We can't really trust that the TCP connections data is valid so tracking of
   * valid heartbeat messages is done internally. While this does not have to
   * be tied to the socket it is the safest and easiest way for this to be tracked
   * that allows for future developments, such as connecting to multiple different
   * TCP servers. */
  if (dataObj.type === 'heartbeat') {
    const time = Math.round((new Date()).getTime() / 1000);
    if (userSocket.lastHeartBeat) {
      const withinTwoSeconds = (time - userSocket.lastHeartBeat) >= 2;
      if (withinTwoSeconds) {
        EventBus.emit(EVENTS.TCP_CLIENT.TIMEOUT, user);
      }
    } else {
      userSocket.lastHeartBeat = time;
    }
  }

  if ((verbose || dataObj.type !== 'heartbeat') && isCurrentFilter) {
    buildObjectAsciiTable(dataObj, user).forEach((table) => {
      CLI.log(table.toString());
    });
    if (timeReponseIsRandomHigh(dataObj)) {
      CLI.log('Random number from time call larger than 30!');
    }
  }
});

EventBus.on(EVENTS.TCP_CLIENT.READ_ERROR, (err, dataBuffer, user) => {
  const badDataString = dataBuffer.toString();
  const isCurrentFilter = isCurrentFilterUser(user);
  if (verbose && isCurrentFilter) {
    CLI.log(`${user} TCP response parsing Error, expected valid JSON but got \n ${badDataString} \n ${err}`);
    return;
  }
  if (isCurrentFilter) {
    CLI.log(`${user} TCP response read error occurred. Use the 'verbose' command to see full details.`);
  }
  CLI.ui.redraw.clear();
});

EventBus.on(EVENTS.TCP_CLIENT.WRITE_ERROR, (err, user) => {
  const isCurrentFilter = isCurrentFilterUser(user);
  if (isCurrentFilter) {
    CLI.log(`An error occured while attempting to write to socket for user ${user}.`, err);
  }
});

EventBus.on(EVENTS.TCP_CLIENT.SOCKET_ERROR, (err, user) => {
  const isCurrentFilter = isCurrentFilterUser(user);
  if (isCurrentFilter) {
    CLI.log(`An error has occured communicating with the socket for user ${user}.`, err);
  }
});

EventBus.on(EVENTS.TCP_CLIENT.SOCKET_CLOSED, (user) => {
  const isCurrentFilter = isCurrentFilterUser(user);
  if (isCurrentFilter) {
    CLI.log(`Socket connection was closed for user ${user}`);
  }
});

EventBus.on(EVENTS.TCP_CLIENT.TIMEOUT, (user) => {
  const isCurrentFilter = isCurrentFilterUser(user);
  if (isCurrentFilter) {
    CLI.log(`Service failed to ping for over 2 seconds, reconnecting and reauthing as ${user}.`);
  }
  try {
    const currentUserSession = getSessionByUser(user);
    destroyTcpListener(currentUserSession);
    removeSession(user);
    EventBus.emit(EVENTS.CLI.USER_LOGIN, user, true);
  } catch (err) {
    CLI.log(`Failed to reauth for user ${user}, you will need to login again for that user.`, err);
  }
});

EventBus.on(EVENTS.CLI.TOGGLE_VERBOSE, () => {
  verbose = !verbose;
});

EventBus.on(EVENTS.CLI.USER_LOGIN, (user, relog = false) => {
  const socket = createTcpListener(new net.Socket(), () => {
    const isCurrentFilter = isCurrentFilterUser(user);
    if (isCurrentFilter) {
      CLI.log(`Waiting for login confirm for ${user}`);
    }
  });
  socket.user = user;
  const eventSocket = attachTcpEventEmitters(socket, user);
  try {
    regSession(user, eventSocket, relog);
    EventBus.emit(EVENTS.TCP_CLIENT.WRITE_TO, [{
      name: user,
    }], user);
  } catch (err) {
    CLI.log(`There was an error logging ${user} in.`);
    CLI.log(err.message);
  }
});

EventBus.on(EVENTS.CLI.USER_LOGOUT, (user) => {
  const isCurrentFilter = isCurrentFilterUser(user);
  const currentUserSession = getSessionByUser(user);
  try {
    destroyTcpListener(currentUserSession);
    removeSession(user);
    if (isCurrentFilter) {
      CLI.log(`Connection session for ${user} closed. User Logged out.`);
    }
  } catch (err) {
    CLI.log(`There was an error logging out the user ${user}`, err);
  }
});

EventBus.on(EVENTS.CLI.USER_NOT_LOGGED_IN, (user) => {
  CLI.log(`User ${user} is not logged in. Please use the 'login' command to due so.`);
});
