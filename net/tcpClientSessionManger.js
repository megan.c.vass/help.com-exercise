const { ERROR_MSGS } = require('../helpers/constants');

const sessions = {};
let currentFilterUser = false;
/**
 * This function that will add to the user session dictionary using
 * the provided username string as the key, and TCP Socket instance as the value.
 * @param  {String} user
 * @param  {net.Socket} tcpClientInstance
 * @return {Boolean}
 */
function regSession(user, tcpClientInstance, replace = false) {
  if (!tcpClientInstance) {
    throw new TypeError(ERROR_MSGS.SESSION_MAN.INVALID_CLIENT);
  }
  if (replace || !sessions[user]) {
    sessions[user] = tcpClientInstance;
  } else {
    throw new Error(ERROR_MSGS.SESSION_MAN.CLIENT_EXISTS);
  }
  return true;
}

/**
 * This function will both close out a Socket connection associated with the
 * username string provided assuming it has been registered and delete the user
 * from the sessions dictionary.
 * @param  {String} user
 * @return {[type]}
 */
function removeSession(user = null) {
  // If user isn't passed than this will fail, along with if a invalid user string
  // is passed in.
  if (!sessions[user]) {
    throw new Error(ERROR_MSGS.SESSION_MAN.CLIENT_MISSING);
  }
  if (user === currentFilterUser) {
    currentFilterUser = false;
  }
  // As nice as delete is. We need to actually remove the key as well, not
  // just set it's value to undefined.
  delete sessions[user];

  return true;
}

/**
 * This function will pause all TCP connections aside from the one associated
 * with the user string provided. If called again with a different key it will
 * simple pause the previous active connection and resume the connection asscoiated
 * with the provided key.
 * @param  {String} user
 * @return {Boolean}
 */
function pauseAllSessionsButUser(user) {
  if (!sessions[user]) {
    throw new Error(ERROR_MSGS.SESSION_MAN.CLIENT_MISSING);
  }
  currentFilterUser = user;
  return true;
}

/**
 * This function will resume all known connections.
 * @return {Boolean}
 */
function resumeAllSessions() {
  currentFilterUser = false;
  return true;
}

/**
 * Simply gets the list of users that currently have a session based on the sessions dictionary.
 * @return {Array}
 */
function getSessionsUserList() {
  return Object.keys(sessions);
}

/**
 * This function simply returns a single socket instance based on the provided user.
 * @return {net.Socket}
 */
function getSessionByUser(user) {
  if (!sessions[user]) {
    throw new Error(`${ERROR_MSGS.SESSION_MAN.CLIENT_MISSING} user ${user}`);
  }
  return sessions[user];
}

/**
 * This function simply returns the current user that is set to be the only user
 * whos request results are written to display.
 * @return {[type]}
 */
function isCurrentFilterUser(user) {
  return (currentFilterUser === false) ? true : (user === currentFilterUser);
}

module.exports = {
  regSession,
  removeSession,
  getSessionsUserList,
  getSessionByUser,
  pauseAllSessionsButUser,
  resumeAllSessions,
  isCurrentFilterUser,
};
