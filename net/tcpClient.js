
const { NET } = require('../config/loader');
const { ERROR_MSGS } = require('../helpers/constants');

/**
 * This method will attempt to create a new TCP socket connection provided that
 * a connection does not already exist and a valid initial connection handler
 * have been provided.
 * @param  {net.Socket} socket
 * @param  {Function} handler
 * @return {net.Socket}
 * @throws {Error}
 */
const createTcpListener = (socket, handler) => {
  if (!socket) {
    throw new Error(ERROR_MSGS.TCP_CLIENT.NO_SOCKET_CRT);
  }
  if (!handler && typeof handler !== 'function') {
    throw new Error(ERROR_MSGS.TCP_CLIENT.NO_HANDLER);
  }
  socket.setTimeout(2000);
  return socket.connect(NET.PORT, NET.ADDR, handler);
};

/**
 * This method will destroy and null out the current TCP client connection.
 * It returns a simple boolean to ensure the result can be evaluated for a
 * successful client destruction that is different than if an error is thrown.
 * @return {net.Socket}
 */
const destroyTcpListener = (client) => {
  if (!client) {
    throw new Error(ERROR_MSGS.TCP_CLIENT.NO_SOCKET_DESTRO);
  }
  // This will keep our handlers intact.
  client.end();
  client.destroy();
  return client;
};

module.exports = {
  createTcpListener,
  destroyTcpListener,
};
