/*
 * Normally a secrets access and loader lib/tool would be used here. But for the
 * pruposes of this exercise this loader will act as our default configuration
 * and env value loader in one.
 */
module.exports = {
  NET: {
    ADDR: process.env.HLP_TCP_ADDR || '35.226.214.55',
    PORT: process.env.HLP_TCP_PORT || '9432',
  },
};
