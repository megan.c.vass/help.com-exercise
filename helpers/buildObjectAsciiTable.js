const AsciiTable = require('ascii-table');

/**
 * This small helper function is a recursive wrapper around the ascii-table
 * library that is used to generate the object output tables for the CLI.
 * @param  {Object} object
 * @param  {String} [childKey=null]
 * @return {Array} An array of tables that can be printed to stdout
 */
const buildObjectAsciiTable = (object, user, childKey = null) => {
  const header = (childKey) ? `Inner Object ${childKey}` : `${user} response`;
  const tables = [];
  const table = new AsciiTable(header);
  Object.keys(object).forEach((key) => {
    if (object[key] && typeof object[key] === 'object' && Object.keys(object[key]).length >= 1) {
      const child = buildObjectAsciiTable(object[key], user, key);
      tables.push(child);
    } else {
      table.addRow(key, object[key]);
    }
  });
  tables.unshift(table);
  return tables;
};
module.exports = {
  buildObjectAsciiTable,
};
