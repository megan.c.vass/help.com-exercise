TCP CLI Tool
=================
This is a small tool used to interact with a dodgy TCP protocol server that was
provided for the purpose of this exercise.

This tool will allow you to interact as best as possible with the server while
maintaining a "logged in" state.

# Quick Start
- `npm i`
- `npm start`

The `npm start` command will drop you into the CLI prompt. From here the CLI tools
`help` command will be run to provide you with the details of the available commands.

You must be "logged in" for most commands to work.
The system will inform you if you need to be logged in for a command when you run one.
In addition, due to the flaky nature of the TCP server the tool will continue
to provide the status of data that is incoming from the server for all sessions
until the `set-filter <user>` command is used to display only traffic for a single
session. *Fatal errors for other connections will still be shown, event with an
active filter*.

*This results in outputs even though you may not be interacting with it at the time.*

Any number of CLI instance can run at a given moment with terminals windows, this
allows a user to represent multiple users even on the same machine, in addition to
multiple users within the same CLI instance.

# Available Commands
This will provide a quick overview of the available commands once you are dropped
into the CLI tools prompt.

*If for any reason a command fails, you will not see the output if it was not parsable
in default mode ( verbose not toggled on ), you will see the following message*
`TCP response read error occurred. Use the 'verbose' command to see full details.`

## `help`
`help` This will display the help menu. The same as one you initially dropped into
the CLI tools prompt.

### Example
```
λ npm start

> help.com-exercise@1.0.0 start D:\workbench\help.com exercise
> node index.js

You must be logged in before any commands will return valid data. Use the 'login' command to due so.
helpcom-tcp$

  Commands:

    help [command...]         Provides help for a given command.
    exit                      Exits application.
    login <user>              Sends a TCP call requesting to be identified as the provided user. You must be logged in for other commands
                              to work.
    logout <user>             Closes the associated users TCP connection by sending the FIN signal. You will be required to login again
                              with that user after.
    get-count <user>          Sends a TCP request asking for the amount of requests made since connection.
    get-time <user>           Sends a TCP request asking for the current time.
    send <user> <jsonString>  Sends a custom JSON payload to the TCP service. Your JSON must be enclosed in single quotes (') and valid.
    verbose                   Toggle verbose mode for ALL users currently logged in. When enabled every network transaction will logged
                              above the CLI tool input as they occur. The stdout will become very busy when enabled.
    set-filter <user>         Will set a user to show only that users TCP responses. Heartbeat results will still be shown for that users
                              connection, and it will not prevent breaking failures from displaying for all connections.
    clear-filter              Will set a user to show only that users TCP responses. Heartbeat results will still be shown.
```
## `exit`
`exit` Simple exits the CLI tools prompt and drops to the terminals default prompt.

## `login`
`login <user>` - This command will allow you to register as a user with the
provided username, the username is required. If the TCP server does not ping the
client within 2 seconds a new connection will be created, and you will be
reauthenticated as the last user.

Issuing the `login` command additional times will create a session for the newly
provided user under the assumption that a session is not already active for the
provided user. After the `login` command has been issued for a user you may use
that user with any of the commands that require a user session.

*If the server is unresponsive it can take a moment for the login confirmation to
be sent to the client. Though in most instances you can issue commands with the
user even if the response has not been received. The client will let you know
if there was a failure sending the login information.*

If for some reason a valid heartbeat is not heard on any socket for over 2 seconds
then a reconnection and reauth will be attempted as well.

### Example
```
helpcom-tcp$ login user1
Waiting for login confirm for user1
.--------------------------.
|      user1 response      |
|--------------------------|
| type | welcome           |
| msg  | Welcome ~~ user1! |
'--------------------------'
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
helpcom-tcp$ login user2
Waiting for login confirm for user2
.--------------------------.
|      user2 response      |
|--------------------------|
| type | welcome           |
| msg  | Welcome ~~ user2! |
'--------------------------'
```
## `logout`
`logout <user>` - This command will send the `FIN` signal from the provided users
associated connection and remove the connection from the systems known ones. If
the logged out user is the active filter than the filter will be reset.

## `get-count`
`get-count <user>` This command will attempt to issue a payload to the server requesting
the number of requests the client has issued since the `login` command has been
issued.

This may also trigger output from a different user to show up if the server pushes
a message to another connected session, this assumes that the `set-filter` command
is not set for a specific user.
### Example 1
```
helpcom-tcp$ get-count user
.---------------.
|   Response    |
|---------------|
| type   | msg  |
| sender | user |
'---------------'
.-----------.
| Inner Object msg |
|-----------|
| count | 3 |
| reply |   |
'-----------'
```

## Example 2
```
helpcom-tcp$ get-count user2
.----------------.
| user2 response |
|----------------|
| type   | msg   |
| sender | user2 |
'----------------'
.-----------.
| Inner Object msg |
|-----------|
| count | 2 |
| reply |   |
'-----------'
.----------------.
| user1 response |
|----------------|
| type   | msg   |
| sender | user2 |
'----------------'
.-----------.
| Inner Object msg |
|-----------|
| count | 2 |
| reply |   |
'-----------'
```

## `get-time`
`get-time <user>` This command will attempt to issue a payload to the server requesting
the current time.

This may also trigger output from a different user to show up if the server pushes
a message to another connected session, this assumes that the `set-filter` command
is not set for a specific user.
### Example 1
```
helpcom-tcp$ get-time user
.---------------.
|   Response    |
|---------------|
| type   | msg  |
| sender | user |
'---------------'
.----------------------------------------.
|            Inner Object msg            |
|----------------------------------------|
| random |                            39 |
| time   | Tue, 10 Sep 2019 20:38:45 GMT |
| reply  |                               |
'----------------------------------------'
Random number from time call larger than 30!
```
### Example 2
```
helpcom-tcp$ get-time user2
.----------------.
| user1 response |
|----------------|
| type   | msg   |
| sender | user2 |
'----------------'
.----------------------------------------.
|            Inner Object msg            |
|----------------------------------------|
| random |                            28 |
| time   | Thu, 12 Sep 2019 08:54:19 GMT |
| reply  |                               |
'----------------------------------------'
.----------------.
| user2 response |
|----------------|
| type   | msg   |
| sender | user2 |
'----------------'
.----------------------------------------.
|            Inner Object msg            |
|----------------------------------------|
| random |                            28 |
| time   | Thu, 12 Sep 2019 08:54:19 GMT |
| reply  |                               |
'----------------------------------------'
```

## `send`
`send <user> '<jsonString>'` This command will attempt to take the provided string and
convert it into a usable JSON structure, that it will then issue to the server.

*The JSON string must be surrounded by single quotes ' , if not it will simply let you know it
was unable to parse it successfully.*

It will only guard against if the JSON provided is valid or not, not if the values
or structure of your JSON means anything to the server.

This may also trigger output from a different user to show up if the server pushes
a message to another connected session, this assumes that the `set-filter` command
is not set for a specific user.
### Example
```
helpcom-tcp$ send user '{"request":"time"}'
{"request":"time"}
.---------------.
|   Response    |
|---------------|
| type   | msg  |
| sender | user |
'---------------'
.----------------------------------------.
|            Inner Object msg            |
|----------------------------------------|
| random |                            29 |
| time   | Tue, 10 Sep 2019 20:39:47 GMT |
| reply  |                               |
'----------------------------------------'
```
## `set-filter`
`set-filter <user>` - This command will filter out all request data, except critical
errors, for all network interactions aside from the provided user related ones.

*If a server timeout occurs the filter will be reset as can be seen in the example*.

### Example
```
helpcom-tcp$ set-filter user1
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
Service failed to ping for over 2 seconds, reconnecting and reauthing as user1.
Waiting for login confirm for user1
.--------------------------.
|      user1 response      |
|--------------------------|
| type | welcome           |
| msg  | Welcome ~~ user1! |
'--------------------------'
user2 TCP response read error occurred. Use the 'verbose' command to see full details.
user1 TCP response read error occurred. Use the 'verbose' command to see full details.
```

## `clear-filter`
`clear-filter` - This command will reset the filter, showing all traffic related
output for all connections.

## `verbose`
`verbose` This command will toggle the amount of information about EVERY request
the server is sending to the client. This can result in some very chaotic out put
but does allow for viewing exactly what the errors are from sending or receiving
data.

# Unit Testing
This project comes a suite of unit tests to ensure it's core functional pieces
are valid and in tact. The suite is written using the Jasmine utility.

To get run the unit tests you will need to install the dev dependencies via
npm, `npm i --only=dev`. Once that is complete you can simply run `npm t` to
run the unit test suite.

### Example
```
λ npm t

> jasmine

Randomized with seed 11085
Started
....................


20 specs, 0 failures
Finished in 0.075 seconds
Randomized with seed 11085 (jasmine --random=true --seed=11085)
```

# A note about multiple lines in TCP responses.
Though the current implementation does not support multiple line responses, to
support this a change would be required in the `parseTcpJsonBuffer` function to
separate out the none carriage line endings ( potentially `\r\n`) producing an array of parsable JSON
lines that could then be stitched back together and parsed as a single object with
numerical keys, or they could be parsed one by one into an array and have that handed
to the READ_SUCCESS event bus listener.

This normally requires a buffer increase on the server a client side for larger
JSON payloads, which is not recommended a majority of the time as it can leave the
TCP protocol vulnerable to attacks and can unexpectedly increase the network footprint
of your application and increase load on the server OS handling multiple client connections.
