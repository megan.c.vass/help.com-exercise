const { fn } = require('../../../cli/commands/toggleVerbose');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the TOGGLE_VERBOSE event to set the CLI verbose flag', () => {
  it('Should emit the TOGGLE_VERBOSE event on the EventBus', (done) => {
    const callables = {
      cmdCallback() {},
      eventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'eventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.TOGGLE_VERBOSE, callables.eventHandler);

    fn({ }, callables.cmdCallback);
    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);

    done();
  });
});
