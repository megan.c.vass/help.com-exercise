const { fn } = require('../../../cli/commands/sendCountReq');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the WRITE_TO event to send the count request', () => {
  it('Should emit the WRITE_TO event on the EventBus with the correct JSON array to parse', (done) => {
    const expectedEmitPayload = [{ request: 'count' }];
    const user = 'test';
    const callables = {
      cmdCallback() {},
      eventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'eventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.TCP_CLIENT.WRITE_TO, callables.eventHandler);

    fn({ user }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledWith(expectedEmitPayload, user);

    done();
  });
});
