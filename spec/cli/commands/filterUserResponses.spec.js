const { fn } = require('../../../cli/commands/filterUserResponses');
const { EventBus } = require('../../../helpers/eventBus');
const { EVENTS } = require('../../../helpers/constants');

describe('Emits the FILTER_USER event to set the user to filter requests by', () => {
  it('Should emit the FILTER_USER event on the EventBus with the an empty string', (done) => {
    const user = 'test';
    const callables = {
      cmdCallback() {},
      eventHandler() {},
    };
    spyOn(callables, 'cmdCallback');
    spyOn(callables, 'eventHandler');
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.FILTER_USER, callables.eventHandler);

    fn({ user }, callables.cmdCallback);

    expect(callables.cmdCallback).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledWith(user);

    done();
  });
});
