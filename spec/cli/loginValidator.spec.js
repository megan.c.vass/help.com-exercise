const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');
// Brought in as objects for easier spy creation.
const { isLoggedInForCommand } = require('../../cli/loggedInValidator');
const { getSessionsUserList, regSession, removeSession } = require('../../net/tcpClientSessionManger');

describe('Emits the USER_NOT_LOGGED_IN event to notify if a provided user is logged in', () => {
  const user = 'test';
  beforeEach(() => {
    try {
      removeSession(user);
    } catch (err) {
      // This is simply to prevent an error about a user not existing on start up.
    }
  });
  it('Should not emit the USER_NOT_LOGGED_IN event on the EventBus with the command provided user is present in tcpClientSessionManger', (done) => {
    const callables = {
      eventHandler() {},
    };
    spyOn(callables, 'eventHandler');

    // Create a valid user cliient
    regSession(user, {});
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.USER_NOT_LOGGED_IN, callables.eventHandler);

    const result = isLoggedInForCommand({ user }, callables.eventHandler);

    // It should call nothing and simply return true if the user is valid.
    expect(result).toEqual(true);
    expect(callables.eventHandler).toHaveBeenCalledTimes(0);

    done();
  });
  it('Should emit the USER_NOT_LOGGED_IN event on the EventBus with the command provided user is not present in tcpClientSessionManger', (done) => {
    const callables = {
      eventHandler() {},
    };
    spyOn(callables, 'eventHandler');

    regSession('test2', {});
    // setup the event listener to intercept the emit.
    EventBus.on(EVENTS.CLI.USER_NOT_LOGGED_IN, callables.eventHandler);

    const result = isLoggedInForCommand({ user }, callables.eventHandler);

    expect(result).toEqual('');
    expect(callables.eventHandler).toHaveBeenCalledTimes(1);
    expect(callables.eventHandler).toHaveBeenCalledWith(user);
    removeSession('test2');
    done();
  });
});
