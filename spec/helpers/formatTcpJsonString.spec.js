const { formatTcpJsonString } = require('../../helpers/formatTcpJsonString');

describe('Object array to string converter', () => {
  it('Should convert a valid JSON object Array with carriage character', (done) => {
    const json = [{ test: 'value' }];
    const jsonStringPayload = formatTcpJsonString(json, true);
    expect(jsonStringPayload.indexOf('\n')).not.toEqual(-1);
    const convertedJson = JSON.parse(jsonStringPayload);
    expect(convertedJson.test).toEqual('value');
    done();
  });
  it('Should convert a valid JSON object Array with no carriage character', (done) => {
    const json = [{ test: 'value' }];
    const jsonStringPayload = formatTcpJsonString(json, false);
    expect(jsonStringPayload.indexOf('\n')).toEqual(-1);
    const convertedJson = JSON.parse(jsonStringPayload);
    expect(convertedJson.test).toEqual('value');
    done();
  });
  it('Should throw an error if a none array of objects is provided', (done) => {
    const json = { test: 'value' };
    try {
      formatTcpJsonString(json, false);
    } catch (err) {
      expect(err).toBeDefined();
    }
    done();
  });
});
