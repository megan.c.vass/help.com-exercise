const { EventBus } = require('../../helpers/eventBus');

describe('EventBus use for pub/sub model communication', () => {
  const eventName = 'testing#channel';

  it('Should be able to create emit lisenters', (done) => {
    const onFunction = () => {};
    const returnedEmitter = EventBus.on(eventName, onFunction);
    // Testing if the emitter listerner was added by checking to see if
    // a chainable emitter was returned.
    expect(typeof returnedEmitter).toEqual('object');
    expect(returnedEmitter.on).toBeDefined();
    done();
  });

  it('Should be able to emit on a channel', (done) => {
    const callables = { onFunction: () => {} };
    spyOn(callables, 'onFunction');
    EventBus.on(eventName, callables.onFunction);
    EventBus.emit(eventName, 'test');
    expect(callables.onFunction).toHaveBeenCalledTimes(1);
    expect(callables.onFunction).toHaveBeenCalledWith('test');
    done();
  });
});
