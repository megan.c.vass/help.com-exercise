const {
  regSession,
  removeSession,
  pauseAllSessionsButUser,
  resumeAllSessions,
  getSessionsUserList,
  getSessionByUser,
  isCurrentFilterUser,
} = require('../../net/tcpClientSessionManger');
const { ERROR_MSGS } = require('../../helpers/constants');

describe('The session manager should be able to store, recover, delete, and report on the state of sessions.', () => {
  const user = 'test';
  beforeEach(() => {
    try {
      removeSession(user);
      // This is admittedly a dirty method of getting rid of extra sessions we create here.
      // But time was a factor for this exercise.
      [1, 2, 3, 4, 5].forEach((num) => removeSession(`${user}${num}`));
    } catch (err) {
      // This is simply to prevent an error about a user not existing on start up.
    }
  });
  it('Should be able to register a new valid session.', (done) => {
    regSession(user, {});
    const sessionList = getSessionsUserList();
    const sessionSocket = getSessionByUser(user);

    expect(sessionList.includes(user)).toEqual(true);
    expect(sessionSocket).toEqual({});
    done();
  });
  it('Should be able to register a new valid session over a old session given the flag.', (done) => {
    regSession(user, {});
    const fakeSocket = { testing: 'value' };
    const sessionList = getSessionsUserList();
    regSession(user, fakeSocket, true);
    const sessionSocket = getSessionByUser(user);
    expect(sessionList.includes(user)).toEqual(true);
    expect(sessionSocket).toEqual(fakeSocket);
    done();
  });
  it('Should not be it able to register a new valid session over a old session without the flag.', (done) => {
    regSession(user, {});
    const fakeSocket = { testing: 'value' };
    const sessionList = getSessionsUserList();
    try {
      regSession(user, fakeSocket);
    } catch (err) {
      expect(err.message).toEqual(ERROR_MSGS.SESSION_MAN.CLIENT_EXISTS);
    }
    const sessionSocket = getSessionByUser(user);
    expect(sessionList.includes(user)).toEqual(true);
    expect(sessionSocket).toEqual({});
    done();
  });
  it('Should be able to remove a session provided a user sessions', (done) => {
    regSession(user, {});
    let sessionList = getSessionsUserList();

    expect(sessionList.includes(user)).toEqual(true);

    removeSession(user);
    sessionList = getSessionsUserList();

    expect(sessionList.includes(user)).toEqual(false);
    done();
  });
  it('Should not be able to remove a session provided a user sessions if it is not found', (done) => {
    regSession(`${user}1`, {});
    const sessionList = getSessionsUserList();
    expect(sessionList.includes(user)).toEqual(false);
    try {
      removeSession(user);
    } catch (err) {
      expect(err.message).toEqual(ERROR_MSGS.SESSION_MAN.CLIENT_MISSING);
    }
    done();
  });
  it('Should be able to pause all current sessions aside from the provided users.', (done) => {
    regSession(user, {});
    pauseAllSessionsButUser(user);
    expect(isCurrentFilterUser(user)).toEqual(true);
    done();
  });
  it('Should be not be able to pause all current sessions aside from the provided users, if the user session is invalid', (done) => {
    try {
      pauseAllSessionsButUser(`${user}2`);
    } catch (err) {
      expect(err.message).toEqual(ERROR_MSGS.SESSION_MAN.CLIENT_MISSING);
    }
    done();
  });
  it('Should be able to resume displaying all current session(s) request info.', (done) => {
    regSession(user, {});
    pauseAllSessionsButUser(user);
    resumeAllSessions();
    expect(isCurrentFilterUser(`${user}1`)).toEqual(true);
    done();
  });
  it('Should be able to return an array of active session keys', (done) => {
    regSession(user, {});
    const userSessionArr = getSessionsUserList();
    expect(userSessionArr.includes(user)).toEqual(true);
    done();
  });
});
