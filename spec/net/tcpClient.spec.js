const {
  createTcpListener,
  destroyTcpListener,
} = require('../../net/tcpClient');
const { ERROR_MSGS } = require('../../helpers/constants');

let callables;
describe('TCP Client Interfaces', () => {
  beforeEach(() => {
    callables = {
      fakeHandler() { },
      // This is a fast mock of the Socket interface found in the 'net' module.
      fakeSocket: {
        setTimeout() {},
        connect(port, addr, handler) {
          handler();
          return {
            destroy: () => {},
            end: () => {},
          };
        },
      },
    };
  });
  // ============ happy Paths ============
  it('Should return a client instance from successfully calling .connect() on the provided socket instance', (done) => {
    spyOn(callables.fakeSocket, 'connect').and.callThrough();
    spyOn(callables, 'fakeHandler');

    const newSocket = createTcpListener(callables.fakeSocket, callables.fakeHandler);
    const socketProps = Object.keys(newSocket);
    expect(socketProps).toContain('destroy');
    expect(callables.fakeSocket.connect).toHaveBeenCalledTimes(1);
    expect(callables.fakeHandler).toHaveBeenCalledTimes(1);
    done();
  });
  it('Should return a client instance whos destroy method has been called.', (done) => {
    spyOn(callables.fakeSocket, 'connect').and.callThrough();
    spyOn(callables.fakeSocket, 'setTimeout');
    spyOn(callables, 'fakeHandler');

    const newSocket = createTcpListener(callables.fakeSocket, callables.fakeHandler);
    const socketProps = Object.keys(newSocket);
    expect(socketProps).toContain('destroy');
    spyOn(newSocket, 'destroy');
    spyOn(newSocket, 'end');
    destroyTcpListener(newSocket);
    expect(newSocket.destroy).toHaveBeenCalledTimes(1);
    expect(newSocket.end).toHaveBeenCalledTimes(1);
    expect(callables.fakeSocket.connect).toHaveBeenCalledTimes(1);
    expect(callables.fakeHandler).toHaveBeenCalledTimes(1);
    expect(callables.fakeSocket.setTimeout).toHaveBeenCalledTimes(1);
    done();
  });
  it('Should return a client instance whos .destroy() method has been called and .connect() has been called again.', (done) => {
    spyOn(callables.fakeSocket, 'connect').and.callThrough();
    spyOn(callables.fakeSocket, 'setTimeout');
    spyOn(callables, 'fakeHandler');

    const newSocket = createTcpListener(callables.fakeSocket, callables.fakeHandler);
    // Emulate the sockets prototype here since we manually mocked the Socket object.
    // This simply ensures the socket object we mocked acts as if it has gone through
    // the connection process and the .destroy() method is present form our mocked return.
    callables.fakeSocket = { ...callables.fakeSocket, ...newSocket };
    expect(callables.fakeSocket.connect).toHaveBeenCalledTimes(1);
    expect(callables.fakeHandler).toHaveBeenCalledTimes(1);
    expect(callables.fakeSocket.setTimeout).toHaveBeenCalledTimes(1);
    done();
  });
  // ============ Error Paths ============
  it('Should throw an error if not provided a socket', (done) => {
    spyOn(callables, 'fakeHandler');
    try {
      createTcpListener(null, callables.fakeHandler);
    } catch (err) {
      expect(err.message).toEqual(ERROR_MSGS.TCP_CLIENT.NO_SOCKET_CRT);
    }
    expect(callables.fakeHandler).not.toHaveBeenCalled();
    done();
  });
  it('Should throw an error if not provided a init connection handler', (done) => {
    spyOn(callables.fakeSocket, 'connect');
    try {
      createTcpListener(callables.fakeSocket, null);
    } catch (err) {
      expect(err.message).toEqual(ERROR_MSGS.TCP_CLIENT.NO_HANDLER);
    }
    expect(callables.fakeSocket.connect).not.toHaveBeenCalled();
    done();
  });
});
