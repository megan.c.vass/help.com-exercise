const { getSessionsUserList } = require('../net/tcpClientSessionManger');
const { EVENTS } = require('../helpers/constants');
const { EventBus } = require('../helpers/eventBus');
/**
 * This function is a wrapper for the TCP client manager to test if we know about
 * the user the command was provided so we can issue a login warning here.
 * @param  {String}  user
 * @return {Boolean}
 */
function isLoggedInForCommand(args) {
  const { user } = args;
  if (!getSessionsUserList().includes(user)) {
    EventBus.emit(EVENTS.CLI.USER_NOT_LOGGED_IN, user);
    return '';
  }
  return true;
}

module.exports = {
  isLoggedInForCommand,
};
