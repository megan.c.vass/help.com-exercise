const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the filter-user command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits cli#filterUserResponse
 */
function setUserToFilterBy(args, callback) {
  const { user } = args;
  EventBus.emit(EVENTS.CLI.FILTER_USER, user);
  callback();
}

module.exports = {
  name: 'set-filter <user>',
  msg: 'Will set a user to show only that users TCP responses. Heartbeat results will still be shown for that users connection, and it will not prevent breaking failures from displaying for all connections.',
  fn: setUserToFilterBy,
};
