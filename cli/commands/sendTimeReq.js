const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the `get-time` vorpal command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits tcpClient#writeTo
 */
function sendTimeReq(args, callback) {
  const { user } = args;
  EventBus.emit(EVENTS.TCP_CLIENT.WRITE_TO, [{
    request: 'time',
  }], user);
  callback();
}

module.exports = {
  name: 'get-time <user>',
  msg: 'Sends a TCP request asking for the current time.',
  fn: sendTimeReq,
};
