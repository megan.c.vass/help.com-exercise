const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the filter-user command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits cli#filterUserResponseClear
 */
function setUserFilterToClear(args, callback) {
  EventBus.emit(EVENTS.CLI.FILTER_USER_CLEAR, '');
  callback();
}

module.exports = {
  name: 'clear-filter',
  msg: 'Will set a user to show only that users TCP responses. Heartbeat results will still be shown.',
  fn: setUserFilterToClear,
};
