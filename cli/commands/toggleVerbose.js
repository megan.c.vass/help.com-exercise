const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the `verbose` vorpal command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits cli#toggleVerbose
 */
function toggleVerbose(args, callback) {
  EventBus.emit(EVENTS.CLI.TOGGLE_VERBOSE);
  callback();
}

module.exports = {
  name: 'verbose',
  msg: 'Toggle verbose mode for ALL users currently logged in. When enabled every network transaction will logged above the CLI tool input as they occur. The stdout will become very busy when enabled.',
  fn: toggleVerbose,
};
