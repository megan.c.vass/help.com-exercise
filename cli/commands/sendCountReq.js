const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the `get-count` vorpal command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits tcpClient#writeTo
 */
function sendCountReq(args, callback) {
  const { user } = args;
  EventBus.emit(EVENTS.TCP_CLIENT.WRITE_TO, [{
    request: 'count',
  }], user);
  callback();
}

module.exports = {
  name: 'get-count <user>',
  msg: 'Sends a TCP request asking for the amount of requests made since connection.',
  fn: sendCountReq,
};
