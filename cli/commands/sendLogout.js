const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the `logout` vorpal command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits cli#userLogout
 */
function sendLogout(args, callback) {
  const { user } = args;
  EventBus.emit(EVENTS.CLI.USER_LOGOUT, user);
  callback();
}

module.exports = {
  name: 'logout <user>',
  msg: 'Closes the associated users TCP connection by sending the FIN signal. You will be required to login again with that user after.',
  fn: sendLogout,
};
