const { EventBus } = require('../../helpers/eventBus');
const { EVENTS } = require('../../helpers/constants');

/**
 * The command handler for the `login` vorpal command.
 * @param  {Object}   args
 * @param  {Function} callback
 * @emits cli#userLogin
 */
function sendLogin(args, callback) {
  const { user } = args;
  EventBus.emit(EVENTS.CLI.USER_LOGIN, user);
  callback();
}

module.exports = {
  name: 'login <user>',
  msg: 'Sends a TCP call requesting to be identified as the provided user. You must be logged in for other commands to work.',
  fn: sendLogin,
};
